package ru.kuzin.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.kuzin.tm.api.repository.dto.IProjectDtoRepository;
import ru.kuzin.tm.dto.model.ProjectDTO;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class ProjectDtoRepository extends AbstractUserOwnedDtoRepository<ProjectDTO> implements IProjectDtoRepository {

    @Override
    protected Class<ProjectDTO> getEntityClass() {
        return ProjectDTO.class;
    }

}